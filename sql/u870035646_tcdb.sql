
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 30, 2016 at 10:28 AM
-- Server version: 10.0.20-MariaDB
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `u870035646_tcdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE IF NOT EXISTS `patient` (
  `patient_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `national_id` bigint(14) NOT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`patient_id`),
  UNIQUE KEY `national_id_UNIQUE` (`national_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`patient_id`, `name`, `national_id`, `birthdate`, `gender`, `creation_date`, `active`) VALUES
(8, 'amr', 14523698745632, '1990-02-25', 'male', '2016-05-29 17:09:44', b'1'),
(6, 'patient1', 12345678910110, '1991-04-10', 'Male', '2016-05-28 23:02:15', b'1'),
(7, 'patient2', 21345678910110, '1991-04-10', 'female', '2016-05-28 23:02:15', b'1'),
(9, 'patient3', 29874120369521, '1990-02-25', 'male', '2016-05-29 17:10:48', b'1'),
(10, 'patient4', 25416987452130, '1999-09-04', 'male', '2016-05-29 18:19:08', b'1'),
(11, 'patient5', 95632147025698, '1998-11-06', 'male', '2016-05-29 18:34:28', b'1'),
(12, 'Sara', 12340987645345, '1990-09-08', 'Female', '2016-05-29 20:36:34', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  `mail` varchar(45) DEFAULT NULL,
  `active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name_UNIQUE` (`user_name`),
  UNIQUE KEY `mail_UNIQUE` (`mail`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `password`, `mail`, `active`) VALUES
(1, 'amr', 'e10adc3949ba59abbe56e057f20f883e', 'amratia4@gmail.com', b'1'),
(10, 'Hesham', '81dc9bdb52d04dc20036dbd8313ed055', 'h.kayal@live.com', b'1'),
(11, 'Sara', '81dc9bdb52d04dc20036dbd8313ed055', 'sara@live.com', b'1'),
(12, 'elzomor13', '5fd32bf4e061dccd0e7fe2bc77ef936c', 'mohamed.ali.elzomor@gmail.com', b'1'),
(6, 'Kayal', 'ce65f40e3a20ad19fe352c52ce3bcf51', 's.kayal@live.com', b'1'),
(7, 'husam', '81dc9bdb52d04dc20036dbd8313ed055', 'husam92@hotmail.com', b'1'),
(13, 'mahmod', '92c23587414f806b41cf3fbb557645df', 'mahmodsaad1991@gmail.com', b'1'),
(14, 'Shady', 'f46ef81f2464441ba58aeecbf654ee41', 'dr.aflato@yahoo.com', b'1'),
(15, 'dr.aflaaatooon', '3c5abfedf9e8acdeae3131bda7532e4d', 'dr.aflaaatooon@gmail.com', b'1'),
(16, 'ddd', 'd0970714757783e6cf17b26fb8e2298f', 'dd@gmail.com', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `vital_signs`
--

CREATE TABLE IF NOT EXISTS `vital_signs` (
  `vital_signs_id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `temperature` float DEFAULT NULL,
  `heart_rate` float DEFAULT NULL,
  `spo2` float DEFAULT NULL,
  `creation_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`vital_signs_id`),
  KEY `patient_vital_signs_idx` (`patient_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `vital_signs`
--

INSERT INTO `vital_signs` (`vital_signs_id`, `patient_id`, `temperature`, `heart_rate`, `spo2`, `creation_date`) VALUES
(1, 6, 38, 80, 55, '2016-05-28 23:41:42'),
(2, 6, 36, 89, 55.22, '2016-05-28 23:41:42');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
